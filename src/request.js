// An awaitable version of making a request
function makeRequest(method, url, body = null) {
    return new Promise(function (resolve, reject) {
        let xhr = new XMLHttpRequest();
        xhr.open(method, url);
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(xhr.response);
            } else {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            }
        };
        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            });
        };
        xhr.send(body);
    });
}

export function get(url) {
    return makeRequest("GET",url);
}

export function del(url) {
    return makeRequest("DELETE",url);
}

export function post(url, payload) {
    return makeRequest("POST",url,payload);
}

export function buildUrl(parts) {
    return window.homeserver + "/_fedichat/v1/json/" + parts;
}
