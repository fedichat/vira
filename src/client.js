import { cache } from "./cache.js";
import * as state from "./state.js";
//import { current_id } from "./main.js";



// This is just a wrapper because I'm not sure the functionality should be in the cache
// it feels super hacky
// this'll make it easier to move later on
export function log(message) {
    cache.add_system_message(message);
    rerender();
}

export function rerender() {
    console.log("Rendering room");
    console.log(cache.render_room(window.current_id));
    document.getElementById("messages").innerHTML = cache.render_room(window.current_id);
    document.getElementById("user_list").innerHTML = cache.render_users(window.current_id);
}

var roomname = "";
var topic = "";
var description = "";

// Update the title and topic for the current room
// If it's changed then log() it
export async function update_current_room_state(should_log = true) {

    // This feels like it should be reduceable
    var new_roomname = (await state.get("roomname")) || window.current_id;
    if (roomname != new_roomname) {
        if (should_log) {
            log("Room name changed to " + new_roomname);
        }
        roomname = new_roomname;
        document.getElementById("room_name").innerHTML = roomname;
    }

    // nbsp is a really dumb hack
    // without it the whole topic element gets optimized out
    // which shifts everything else on the page
    var new_topic = (await state.get("topic")) || "&nbsp;";
    if (topic != new_topic) {
        if (should_log) {
            log("Topic changed to " + new_topic);
        }
        topic = new_topic;
        document.getElementById("room_topic").innerHTML = topic;
    }

    var new_description = (await state.get("description")) || "This room seems unassuming.";
    if (description != new_description) {
        if (should_log) {
            log("Description changed to " + new_description);
        }
        description = new_description;
    }
}

