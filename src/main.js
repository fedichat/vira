import * as request from "./request.js";
import * as sender from "./sender.js";
import * as state from "./state.js";

import { cache } from "./cache.js";
import { rerender,update_current_room_state } from "./client.js";

// Settings
// TODO: move this into a config module
const poll_ms = 100;
const data_poll_ms = 1000;

// Globals
window.homeserver = "";
window.username = "";

// should this be part of the cache?
window.current_id = "";

//data
//
// message cache
//   map of id -> message list
//
//   not quite
//   each room isn't just a message list
//   it also has a highlight flag
//
//   cache should be in its own file I think
//
//   hmm how do I do globals with `import`
// var cache = {};

// memorized rooms
//   map of name -> id
var favorites = {};

// room name, topic, and description
export var room_name = "";
export var topic = "";
export var description = "";


function init() {
    // TODO: should defaults be constants?
    window.username = localStorage.getItem("username") ?? prompt("Please enter your username. You can change it later with /nick");
    // save username in case it changed
    localStorage.setItem("username",window.username);
    window.homeserver = localStorage.getItem('homeserver') ?? "https://fedichat.firechicken.net";
    window.current_id = localStorage.getItem('currentId') ?? "1,1";
    update_current_room_state(false);
}


// MAIN LOOP
document.addEventListener('DOMContentLoaded', async function(){ 
    // if username is empty start up prompt
    // and do other init things
    init();
    //
    // start main loop
    //
    // there's a couple loops
    // a rendering loop
    //   wait how often do we need to render?
    //   only when the state changes
    //   so render when
    //   - new message
    //   - topic/name changes
    //   - every minute or so to update recent users + their time
    //   - really though I only need to update individual components
    //
    //   RENDERING COMPONENTS
    //   - messages
    //   - room list
    //   - user list
    //   - minimap
    //
    // a low-latency message fetching loop
    // 1000 ms for now
    setInterval(async function () {
        var new_messages = await cache.update_room_messages(window.current_id);
        console.log(new_messages);
        if (new_messages != null && new_messages.length != 0) {
            rerender();
        }
    }, poll_ms);
    // a high latency room data sync
    setInterval(async function () {
        update_current_room_state();
    }, data_poll_ms);
    
    // set up input box listener
    document.getElementById("input_form").onsubmit = function() { 
        sender.submitMessage(document.getElementById('text_input').value);
        document.getElementById('text_input').value = "";
        return false;
    };
}, false);
