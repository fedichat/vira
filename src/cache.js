import * as request from "./request.js";
import * as message from "./message.js";

import { log } from "./client.js";

const room = {
    new: function () {
        return {
            messages: []
        }
    }
}

export var cache = {};
// list of rooms
cache.rooms = []
// and methods
//
// each room has
//   - messages
//   - flags
//   - maybe a portion of the state?


// Fetch all messages in the current room and rooms in the favorites list
cache.update_room_messages = async function (room_id) {
    var response;
    try {
        if (cache.rooms[room_id] == null) {
            cache.rooms[room_id] = room.new();
            // TODO: this should only fetch N messages
            // but how large should N be?
            const url = window.homeserver + "/_fedichat/v1/json/room/" + room_id + "/messages?limit=500"
            response = await request.get(url);
        } else {
            const max_id = Math.max.apply(Math, cache.rooms[room_id].messages.map(function(o) { return o.message_id; }));
            const url = window.homeserver + "/_fedichat/v1/json/room/" + room_id + "/messages?after=" + max_id;
            // Okay should this be synchronous? Should I await?
            response = await request.get(url);
        }
    } catch (err) {
        // is this leaking memory
        if (cache.rooms[room_id].messages.length == 0) {
            log("Nothing here yet.");
        }
        return;
    }

    // Check if `messages` has an error value
    // otherwise it's a list of messages
    const parsed_messages = JSON.parse(response);

    // dictionary == error
    if (parsed_messages.constructor == Object) {
        log("Server error: " + parsed_messages.description);
        // Should this return the error?
        return null;
    } else {
        // We have a list of messages
        // merge them into what we have in the cache
        // MAYBE TODO: sort by id
        cache.rooms[room_id].messages = parsed_messages.concat(cache.rooms[room_id].messages);
        return parsed_messages;
    }
}

// Render a room into an html string
cache.render_room = function (room_id) {
    if (cache.rooms[room_id] == null) {
        return null;
    } else {
        return cache.rooms[room_id].messages
            .map(m => message.render_message(m))
            .join("\n");
    }
}

// Render the users present in a room
// NOTE: This shows all visible users
// not necessarily active/online users
cache.render_users = function (room_id) {
    if (cache.rooms[room_id] == null) {
        return null;
    } else {
        // deduplicate users
        // and render
        // stackoverflow told me set good
        return [...new Set(cache.rooms[room_id].messages
            .filter(m => (m.from != "*"))
            .map(m => ("<span class=\"block\">" + m.from + "</span>")))]
            .sort()
            .join("\n");
    }

}

// Add a log message or something to show the user
// this feels so hacky
cache.add_system_message = function (text) {
    if (cache.rooms[window.current_id] == null) {
        cache.rooms[window.current_id] = room.new();
    }
    // oh god if this has a type
    // people can send messages with the type
    // should I make it untyped or sanitize the type when receiving new messages
    cache.rooms[window.current_id].messages.unshift({from: "*", body: text, type: "system", message_id: -1})
}
